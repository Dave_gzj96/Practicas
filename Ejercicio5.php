<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet"  href="css/delimeters.css">
	<link rel="stylesheet" type="text/css" href="css/tabs.css">
	<title>ejercicio 5</title>
</head>
<body>
<h1>Ejemplo de operaciones comparacion en PHP</h1>
	
			<?php
			$a = 8;
			$b = 3;
			$c = 3;
			echo $a == $b, "<br>";
			echo $a != $b, "<br>";
			echo $a < $b, "<br>";
			echo $a > $b, "<br>";
			echo $a >= $c, "<br>";
			echo $a <= $c, "<br>";			
		/*
		========= Anote el significado de las operaciones de comparacion: ================

		Operador == "IGUALACION"
		Operador != "DIFERENTE/DISTINTO DE"
		Operador < "MENOR QUE"
		Operador > "MAYOR QUE"
		Operador >= "MAYOR O IGUAL A"
		Operador <= "MENOR O IGUAL A"
					
		*/
		?>

</body>
</html>