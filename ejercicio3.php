<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet"  href="css/delimeters.css">
	<link rel="stylesheet" type="text/css" href="css/tabs.css">
	<title>ejercicio3</title>
</head>
<body>
				<?php 

					$un_bool = TRUE; //valor booleano
					$un_str ="programacion"; //valor de cadena
					$un_str2 = 'progrmacion'; //valor de cadena
					$un_int = 12; //valor entero

					echo gettype ($un_bool); //imprime "boolean"
					echo gettype ($un_str); //imprime "string"

					// si el valor es entero lo incrementa en cuatro 	
					if (is_int($un_int)) {
						$un_int +=4;
					}

					/*
					si es $bool es una cadena, la imprime 
					(no impreme nada) 
					*/
					if (is_string($un_bool)) {
						echo "Cadena: $un_bool";
					}

				 ?>

</body>
</html>