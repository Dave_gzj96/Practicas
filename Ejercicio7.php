<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet"  href="css/delimeters.css">
	<link rel="stylesheet" type="text/css" href="css/tabs.css">
	<title>ejercicio 7</title>
</head>
<body>
	<h1>Resuelva las expresiones presentadas</h1>

		<?php
			$i = 9;
			$f = 33.5;
			$c = 'X';
			
			
			echo ($i >= 6) && ($c == 'X'), "<br>";
			echo ($i >= 6) || ($c == 12), "<br>";
			echo ($f < 11) && ($i > 100), "<br>";
			echo ($c != 'P') || (($i + $f) <= 10), "<br>";
			echo  $i + $f <= 10 ,"<br>";
			echo  $i >= 6 && $c == 'X', "<br>";
			echo  $c != 'P' || $i + $f <= 10,  "<br>";
					
		/*

		========================================================
		|	Expresion       	|	Resultado   |
		=======================================================			
		| ($i >= 6) && ($c == 'X')         |     1  |		   	
		| ($i >= 6) || ($c == 12)          |     1  |  	  	  	
		| ($f < 11) && ($i > 100)                
		| ($c != ‘P’) || (($i + $f) <= 10) |     1  |
		| $i + $f <= 10					 
		| $i >= 6 && $c == ‘X’             |     1  | 
		| $c != ‘P’ || $i + $f <= 10       |     1  |
		=========================================================
		
		*/

		?>

</body>
</html>