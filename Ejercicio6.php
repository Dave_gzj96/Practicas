<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet"  href="css/delimeters.css">
	<link rel="stylesheet" type="text/css" href="css/tabs.css">
	<title>ejercicio 6</title>
</head>
<body>
<h1>Ejemplo de operaciones logicas en PHP</h1>
	
		<?php

			$a = 8;
			$b = 3;
			$c = 3;
			
			echo ($a == $b) && ($c > $b), "<br>";
			echo ($a == $b) || ($b == $c), "<br>";
			echo !($b <= $c)'b', "<br>";
			/*
			Anote el significado de las operaciones de comparacion
			Operador de Comparacion &&: Es el condicional "y" y arroja un resultado unicamente si ambas condiciones se cumplen 
			Operador de Comparacion ||: Es el condicional "o" y arroja un resultado si cualquiera de las 2 condiciones se cumplen

			*/

		?>

</body>
</html>